<?php
/* Route::get('/', function () {
    return view('welcome');
}); */

//Auth::routes();

Route::get('{slug}', function() {
    return view('home');
})->where('slug', '(?!api|admin)([A-z\d-\/_.]+)?');

Route::group(['prefix' => 'admin'], function () {
	Route::get('/', 'Auth\LoginController@showLoginForm');
	Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
	Route::post('/login', 'Auth\LoginController@login')->name('login');
	Route::post('/logout', 'Auth\LoginController@logout')->name('logout');
	Route::get('/dashboard', 'Pages\MainController@index')->name('dashboard');
	Route::get('/user/getdata', 'Pages\UserController@getData')->name('user.data');
	Route::resource('user','Pages\UserController');
	Route::get('/client/getdata', 'Pages\ClientController@getData')->name('client.data');
	Route::get('/client/{client_id}/users', 'Pages\ClientController@getUsers');
	Route::resource('client','Pages\ClientController');
	Route::get('/staff/getdata', 'Pages\StaffController@getData')->name('staff.data');
	Route::resource('staff','Pages\StaffController');
	Route::get('/document/getdata', 'Pages\DocumentController@getData')->name('document.data');
	Route::resource('document','Pages\DocumentController');
	Route::get('/chat', 'Pages\ChatController@index')->name('chat.index');
	Route::post('/chat/{document}', 'Pages\ChatController@store')->name('chat.store');
	Route::get('/chat/{document}', 'Pages\ChatController@getData')->name('chat.data');
	Route::delete('/chat/{document}', 'Pages\ChatController@destroy')->name('chat.destroy');
	Route::get('/message/getdata', 'Pages\ChatController@getMessageData')->name('message.data');
	Route::get("/chat/count/data",'Pages\ChatController@chatCount')->name('chat.count');
	Route::post('/document/{document}/status', 'Pages\DocumentController@changeStatus')->name('document.status');
});