<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix'=> 'auth'],function(){
    Route::post("/login",'Auth\LoginController@frontLogin');
});

Route::group(['prefix'=> 'document'],function(){
    Route::post("/",'Front\DocumentController@store');
    Route::get("/",'Front\DocumentController@data');
    Route::get("/{id}",'Front\DocumentController@featchData');
	Route::post("/{document}",'Front\DocumentController@update');
	Route::delete("/{id}",'Front\DocumentController@destroy');
	Route::delete("/file/{document}/{file}",'Front\DocumentController@deleteFile');
});

Route::group(['prefix'=> 'chat'],function(){
    Route::get("/{document}",'Front\ChatController@featchData');
    Route::post("/{document}",'Front\ChatController@store');
    Route::get("/count/data",'Front\ChatController@chatCount');
	Route::get("/unread/message",'Front\ChatController@unreadMessage');
});

Route::get("/documents",'Front\DocumentController@featchDocuments');