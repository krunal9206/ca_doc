<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author = Role::create([
            'name' => 'Super Admin', 
            'slug' => 'super',
            'permissions' => []
        ]);
        $editor = Role::create([
            'name' => 'Admin', 
            'slug' => 'admin',
            'permissions' => []
        ]);
		 $editor = Role::create([
            'name' => 'User', 
            'slug' => 'user',
            'permissions' => []
        ]);
    }
}
