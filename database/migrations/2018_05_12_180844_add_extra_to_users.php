<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtraToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
			$table->string('username', 20)->unique()->after('email');
			$table->enum('business_type', array('Individual', 'Partnership Firm', 'LLP', 'Company'))->nullable()->after('status');
			$table->string('name_ppd', 50)->nullable()->after('business_type');
			$table->string('pan_no', 50)->nullable()->after('name_ppd');
			$table->string('gst_no', 50)->nullable()->after('pan_no');
			$table->string('mobile', 20)->nullable()->after('gst_no');
			$table->text('address')->nullable()->after('mobile');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
			$table->dropColumn('username');
			$table->dropColumn('business_type');
			$table->dropColumn('name_ppd');
			$table->dropColumn('pan_no');
			$table->dropColumn('gst_no');
			$table->dropColumn('mobile');
			$table->dropColumn('address');
		});
    }
}
