import Http from '../Http'
import * as action from '../store/actions'

export function store(fields) {
	var formData = new FormData();
	formData.append("upload_type", fields.upload_type);
	formData.append("document_type", fields.document_type);
	
	var files = fields.document_file;
	for (var i = 0; i < files.length; i++) {
		formData.append("document_file["+i+"]", files[i])
	}
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.post('api/document', formData)
                .then(res => {
                    //dispatch(action.createDocument(res.data));
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.message,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function data(page_size, page, sorted, filtered) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.get('../api/document?page_size=' + page_size + '&page=' + (page + 1) + '&sorted=' + JSON.stringify(sorted) + '&filtered=' + JSON.stringify(filtered))
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.message,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function featchData(id) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.get('../api/document/' + id)
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.error,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function update(fields, id) {
	var formData = new FormData();
	formData.append("name", fields.name);
	formData.append("document_type", fields.document_type);
	
	var files = fields.document_file;
	for (var i = 0; i < files.length; i++) {
		formData.append("document_file["+i+"]", files[i])
	}
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.post('../api/document/' + id, formData)
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.message,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function deleteData(id) {
	return dispatch => (
        new Promise((resolve, reject) => {
            Http.delete('../api/document/' + id)
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.message,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function deleteFile(doc, id) {
	return dispatch => (
        new Promise((resolve, reject) => {
            Http.delete('../api/document/file/' + doc + '/' + id)
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.message,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function featchAllData(page, pageSize, document_type) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.get('../api/documents?page=' + page + '&page_size=' + pageSize + '&document_type=' + document_type)
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.message,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}