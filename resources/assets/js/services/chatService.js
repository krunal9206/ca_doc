import Http from '../Http'
import * as action from '../store/actions'

export function store(fields, id) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.post('../api/chat/' + id, fields)
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.message,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function featchData(id) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.get('../api/chat/' + id)
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.error,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function unreadData(page_size, page) {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.get('../api/chat/unread/message?page_size=' + page_size + '&page=' + (page + 1))
                .then(res => {
                    return resolve(res.data);
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.message,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}

export function chatCount() {
    return dispatch => (
        new Promise((resolve, reject) => {
            Http.get('../api/chat/count/data')
                .then(res => {
					dispatch(action.chatCount(res.data));
                    return resolve();
                })
                .catch(err => {
                    const statusCode = err.response.status;
                    const data = {
                        error: err.response.data.error,
                        statusCode,
                    };
                    return reject(data);
                })
        })
    )
}