import * as ActionTypes from '../action-types'
import Http from '../../Http'

const initialState = {
    chat_count : 0,
};

const Chat = (state= initialState,{type,payload = null}) => {
    switch(type){
        case ActionTypes.CHAT_COUNT:
            return chatCount(state,payload);
        default:
            return state;
    }
};

const chatCount = (state,payload) => {
    const chat_count = payload.data;
    state = Object.assign({}, state, {
        chat_count: chat_count,
    });
    return state;

};



export default Chat;
