import {combineReducers} from 'redux'
import Auth from './Auth'
import Chat from './Chat'
import persistStore from './persistStore'

const RootReducer = combineReducers({Auth, Chat, persistStore});

export default RootReducer;