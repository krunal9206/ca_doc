import React from 'react'
import {
    Button, Divider, Dimmer, Form, Grid, Header, Icon, Loader, Message, Segment, Container, Modal, Label, Pagination, Image
} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import PageHeader from '../../common/pageHeader'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import * as DocumentService from '../../services/documentService'
import * as ChatService from '../../services/chatService'
import Wcst from '../../wcst'

class Page extends React.Component {
    constructor(props) {
        super(props);
		
		this.state = {
			document_type: '',
			files: [],
			page: 1,
			pageSize: 24,
			isLoading: false,
		};
		this.handleChange = this.handleChange.bind(this);
		this.handlePaginationChange = this.handlePaginationChange.bind(this);
		this.fetchData = this.fetchData.bind(this);
    }
	
	fetchData() {
		this.setState({
			isLoading: true
		});
		this.props.dispatch(DocumentService.featchAllData(this.state.page, this.state.pageSize, this.state.document_type)).then((res) => {
			this.setState({
				files: res.data,
				isLoading: false
            });
		});
	}
	
	handleChange(event) {
		this.setState({
			document_type: event.target.value,
			page: 1,
		}, function () {
			this.fetchData();
		});
    }
	
	handlePaginationChange(event, data) {
		this.setState({
			page: data.activePage
		}, function () {
			this.fetchData();
		});
    }
	
	componentDidMount(){
        this.fetchData();
    }
  

    render() {
		if(this.state.files.data){
			var document_file_html = [];
			this.state.files.data.map(file => {
			var ext = file.file_name.split(".").pop();
			if(ext == 'pdf'){
				var file_url  = Wcst.server_url + 'dist/img/doc-icon.png';
			} else {
				var file_url  = Wcst.server_url + 'uploads/' + file.file_name;
			}
			document_file_html.push(
				<Grid.Column key={file.id}>
						<Segment><a href={Wcst.server_url + 'uploads/' + file.file_name} target="_blank"><Image src={file_url} /></a></Segment>
				</Grid.Column>
			);
    });
		}
		const { files, page } = this.state;
		
        return (
            <div>
                <PageHeader heading="Documents"/>
				<div className="course-tour">
				<Container className="step-container">
                            <Grid columns={1} padded="horizontally">
                                <Grid.Row>
                                    <Grid.Column>
										<Header as='h4' block>
											Documents
											<select name='document_type' style={{marginLeft: '10px'}} onChange={this.handleChange} value={this.state.document_type}>
												<option value="">All</option>
												<option value="Purchase Bill">Purchase Bill</option>
												<option value="Sales Bill">Sales Bill</option>
												<option value="Expense Bill">Expense Bill</option>
												<option value="Deposit Slip">Deposit Slip</option>
												<option value="Payment">Payment</option>
												<option value="Others">Others</option>
											</select>
											<Button size='mini' as={Link} to={'/dashboard/'} negative floated="right">Back</Button>
										</Header>
										<Segment className='page-loader' style={{display: this.state.isLoading ? 'block' : 'none'}}>
											<Dimmer active inverted>
												<Loader size='large'>Loading...</Loader>
											</Dimmer>
										</Segment>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
							<Grid columns={1} padded="horizontally" className="doc-file">
								<Grid.Row columns={6}>{document_file_html}</Grid.Row>
								{files.last_page > 1 && <Pagination activePage={page} onPageChange={this.handlePaginationChange} totalPages={files.last_page} />}
							</Grid>
                    </Container>
				</div>
            </div>
        );
    }
}

export default Page;