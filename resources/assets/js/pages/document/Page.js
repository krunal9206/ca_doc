import React from 'react'
import {
    Button, Divider, Dimmer, Form, Grid, Header, Icon, Loader, Message, Segment, Container, List, Image
} from 'semantic-ui-react'
import { browserHistory } from 'react-router'
import {Link, Redirect} from 'react-router-dom'
import PageHeader from '../../common/pageHeader'
import * as DocumentService from '../../services/documentService'
import Validator from 'ree-validate'
import Wcst from '../../wcst'

class Page extends React.Component {
    constructor(props) {
        super(props);
		
		this.validator = new Validator({
            document_type: 'required',
            upload_type: 'required',
        });

        this.state = {
            fields: {
                document_type: '',
                upload_type: '',
                document_file: '',
            },
            responseError: {
                isError: false,
                code: '',
                text: ''
            },
			id: this.props.match.params.id,
            isLoading: false,
            isRedirect: false,
            errors: this.validator.errors,
            imageError: false,
			files: []
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleFileChange = this.handleFileChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
		
		if (this.state.id) {
			this.featchData(this.state.id);
			this.removeDocument = this.removeDocument.bind(this);
		}
    }
	
	featchData(id) {
		this.props.dispatch(DocumentService.featchData(id))
		.then((response) => {
				this.setState({
					fields: {
						document_type: response.data.document_type,
						document_file: '',
					},
					files: response.data.files
                });
		})
		.catch(({error}) => {
			if(error == 'invalid_data'){
				this.props.history.push('/dashboard');
			}
		})
	}
	
	handleChange(event) {
        const name = event.target.name;
		const value = event.target.value;
		
        const {fields} = this.state;
        fields[name] = value;
        this.validator.validate(name, value)
            .then(() => {
                const {errors} = this.validator;
                this.setState({errors, fields})
            })
    }
	
	handleFileChange(event) {
        const name = event.target.name;
		const value = event.target.files;
		
        const {fields} = this.state;
        fields[name] = value;
    }

    handleSubmit(event) {
        event.preventDefault();
        const {fields} = this.state;
		
        this.validator.validateAll(fields)
            .then(success => {
                if (success) {
					if(this.state.id){
						this.setState({
							isLoading: true
						});
						this.submit(fields);
					} else {
						if(fields.document_file.length > 0){
							this.setState({
								isLoading: true
							});
							this.submit(fields);
						} else {
							this.setState({
								imageError: true
							});
						}
					}
                } else {
					const {errors} = this.validator;
					this.setState({errors, fields})
				}
            });
    }

    submit(fields) {
		if (this.state.id) {
			this.props.dispatch(DocumentService.update(fields, this.state.id))
			.then((response) => {
				this.setState({
					isRedirect: true
				});
			})
            .catch(({error, statusCode}) => {
                const responseError = {
                    isError: true,
                    code: statusCode,
                    text: error
                };
                this.setState({responseError});
                this.setState({
                    isLoading: false
                });
            })
		} else {
			this.props.dispatch(DocumentService.store(fields))
			.then((response) => {
				this.setState({
					isRedirect: true
				});
			})
            .catch(({error, statusCode}) => {
                const responseError = {
                    isError: true,
                    code: statusCode,
                    text: error
                };
                this.setState({responseError});
                this.setState({
                    isLoading: false
                });
            })
		}

    }
	
	componentDidMount(){
        this.setState({
            isLoading: false
        });
    }
	
	removeDocument(event) {
		const id = event.target.value;
		this.setState({
            isLoading: true
        });
		this.props.dispatch(DocumentService.deleteFile(this.state.id, id))
		.then((response) => {
			const files = this.state.files;
			const index = files.findIndex(a => a.id == id);
			
			files.splice(index, 1);
			
			this.setState({
				files: files,
				isLoading: false
			});
		})
	}

    render() {
		if(this.state.isRedirect){
			return (
                <Redirect to='/dashboard'/>
            )
		}
		const {errors} = this.state;
		
		if(this.state.id && this.state.files){
			var document_file_html = [];
			this.state.files.map(file => {
			var ext = file.file_name.split(".").pop();
			if(ext == 'pdf'){
				var file_url  = Wcst.server_url + 'dist/img/doc-icon.png';
			} else {
				var file_url  = Wcst.server_url + 'uploads/' + file.file_name;
			}
			document_file_html.push(
				<List.Item key={file.id}>
					<List.Content floated='right'>
						<Button onClick={this.removeDocument} value={file.id}>Delete</Button>
					</List.Content>
					<Image size='mini' src={file_url} />
					<List.Content>{file.ori_name}</List.Content>
				</List.Item>
			);
    });
		}
		
        return (
            <div>
                <PageHeader heading="Document"/>
				<div className="course-tour">
				<Container className="step-container">
                            <Grid columns={1} padded="horizontally">
                                <Grid.Row>
                                    <Grid.Column>
										<Header as='h3' block>Upload your document here.</Header>
										<Segment className='page-loader' style={{display: this.state.isLoading ? 'block' : 'none'}}>
                    <Dimmer active inverted>
                        <Loader size='large'>Loading...</Loader>
                    </Dimmer>
                </Segment>
				
				{this.state.responseError.isError && <Message negative>
                            <Message.Content>
                                {this.state.responseError.text}
                            </Message.Content>
                        </Message>}
                            <Form>
								<div className="field">
									<label>Document Type</label>
									<select name='document_type' onChange={this.handleChange} value={this.state.fields.document_type}>
										<option value="">Select</option>
										<option value="Purchase Bill">Purchase Bill</option>
										<option value="Sales Bill">Sales Bill</option>
										<option value="Expense Bill">Expense Bill</option>
										<option value="Deposit Slip">Deposit Slip</option>
										<option value="Payment">Payment</option>
										<option value="Others">Others</option>
									</select>
								</div>
								{errors.has('document_type') && <Header size='tiny' className='custom-error' color='red'>
                                    {errors.first('document_type')}
                                </Header>}
								
								<div className="field">
									<label>Upload Type</label>
									<select name='upload_type' onChange={this.handleChange} value={this.state.fields.upload_type} disabled={this.state.id ? true : false}>
										<option value="">Select</option>
										<option value="Single Upload">Single Upload</option>
										<option value="Bulk Upload">Bulk Upload</option>
									</select>
								</div>
								{errors.has('upload_type') && <Header size='tiny' className='custom-error' color='red'>
                                    {errors.first('upload_type')}
                                </Header>}
								
								<div className="field">
									<label>Files</label>
									<input name="document_file" type="file" accept=".jpg,.jpeg,.png,.pdf" multiple={true} onChange={this.handleFileChange} />
								</div>
								{this.state.imageError && <Header size='tiny' className='custom-error' color='red'>
                                    File is required.
                                </Header>}
								
								{this.state.files && <List celled>
								{document_file_html}
								</List>}
								
                                <Button positive onClick={this.handleSubmit}>{this.state.id ? 'Update' : 'Submit'}</Button>
								<Button negative as={Link} to="/dashboard" floated='right'>Back</Button>
							</Form>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                    </Container>
				</div>
            </div>
        );
    }
}

export default Page;