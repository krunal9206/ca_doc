import React from 'react'
import {
    Button, Divider, Dimmer, Form, Grid, Header, Icon, Loader, Message, Segment, Container, List, Image, Table, Comment
} from 'semantic-ui-react'
import { browserHistory } from 'react-router'
import {Link, Redirect} from 'react-router-dom'
import PageHeader from '../../common/pageHeader'
import * as DocumentService from '../../services/documentService'
import * as ChatService from '../../services/chatService'
import Validator from 'ree-validate'
import Wcst from '../../wcst'

class Page extends React.Component {
    constructor(props) {
        super(props);
		
		this.validator = new Validator({
            message: 'required',
        });

        this.state = {
			fields: {
                message: '',
            },
			responseError: {
                isError: false,
                code: '',
                text: ''
            },
			id: this.props.match.params.id,
			data: [],
			chat_data: [],
            isLoading: true,
            errors: this.validator.errors,
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
		this.featchData(this.state.id);
    }
	
	featchData(id) {
		this.props.dispatch(DocumentService.featchData(id))
		.then((response) => {
				this.setState({
					isLoading: false,
					data: response.data
				});
		})
		.catch(({error}) => {
			if(error == 'invalid_data'){
				this.props.history.push('/dashboard');
			}
		})
	}
	
	async componentDidMount() {
          this.chat = setInterval(async () => {
              const res = await fetch('../api/chat/' + this.state.id + '?token=' + localStorage.getItem('jwt_token'));
              const chats = await res.json();

              this.setState({
                chat_data: chats.data,
              })
            }, 2000);
    }
	
	componentWillUnmount() {
		clearInterval(this.chat);
	}
	
	componentDidUpdate (){
        if (this.refs.chatoutput != null) {
            this.refs.chatoutput.scrollTop = this.refs.chatoutput.scrollHeight;
        }
    }
	
	handleChange(event) {
        const name = event.target.name;
		const value = event.target.value;
		
        const {fields} = this.state;
        fields[name] = value;
        this.validator.validate(name, value)
            .then(() => {
                const {errors} = this.validator;
                this.setState({errors, fields})
            })
    }

    handleSubmit(event) {
        event.preventDefault();
        const {fields} = this.state;
		
        this.validator.validateAll(fields)
            .then(success => {
                if (success) {
					this.setState({
						isLoading: true
					});
					this.submit(fields);
                } else {
					const {errors} = this.validator;
					this.setState({errors, fields})
				}
            });
    }

    submit(fields) {
			this.props.dispatch(ChatService.store(fields, this.state.id))
			.then((response) => {
				this.setState({
                    isLoading: false,
					fields: {
						message: '',
					},
                });
			})
            .catch(({error, statusCode}) => {
                const responseError = {
                    isError: true,
                    code: statusCode,
                    text: error
                };
                this.setState({responseError});
                this.setState({
                    isLoading: false
                });
            })
    }

    render() {
		const {errors} = this.state;
		
		if(this.state.id && this.state.data.files){
			var document_file_html = [];
			this.state.data.files.map(file => {
			var ext = file.file_name.split(".").pop();
			if(ext == 'pdf'){
				var file_url  = Wcst.server_url + 'dist/img/doc-icon.png';
			} else {
				var file_url  = Wcst.server_url + 'uploads/' + file.file_name;
			}
			document_file_html.push(
				<Grid.Column key={file.id}>
						<Segment><a href={Wcst.server_url + 'uploads/' + file.file_name} target="_blank"><Image src={file_url} /></a></Segment>
				</Grid.Column>
			);
    });
		}
		
		if(this.state.id && this.state.chat_data){
			var chat_data = [];
			this.state.chat_data.map(chat => {
				
				if(chat.user_id == this.props.UserID){
					var avatar_url = Wcst.server_url + 'dist/img/user-chat.png';
					var class_name = 'wcst-chat-user';
				} else {
					var avatar_url = Wcst.server_url + 'dist/img/admin-chat.png';
					var class_name = 'wcst-chat-admin';
				}
				chat_data.push(
					<Comment key={chat.id}>
						  <Comment.Avatar src={avatar_url} />
						  <Comment.Content>
							<Comment.Author as='a'>{chat.user.name}</Comment.Author>
							<Comment.Metadata>
							  <div>{chat.created_at}</div>
							</Comment.Metadata>
							<Comment.Text><div className={class_name}>{chat.message}</div></Comment.Text>
						  </Comment.Content>
					</Comment>
				);
    });
		}
		
        return (
            <div>
                <PageHeader heading="Chat"/>
				<div className="course-tour">
				<Container className="step-container">
				<Grid columns={1} padded="horizontally">
					<Grid.Row>
					<Grid.Column>
						<Header as='h4' block>
						Documents and chat.
							<Button size='mini' as={Link} to={'/dashboard/'} negative floated="right">Back</Button>
						</Header>
						<Segment className='page-loader' style={{display: this.state.isLoading ? 'block' : 'none'}}>
							<Dimmer active inverted>
								<Loader size='large'>Loading...</Loader>
							</Dimmer>
						</Segment>
				
						{this.state.responseError.isError && <Message negative>
                            <Message.Content>
                                {this.state.responseError.text}
                            </Message.Content>
                        </Message>}
						
						<Table celled>
							<Table.Header>
							  <Table.Row>
								<Table.HeaderCell>Name</Table.HeaderCell>
								<Table.HeaderCell>Type</Table.HeaderCell>
								<Table.HeaderCell>Created At</Table.HeaderCell>
								<Table.HeaderCell>Updated At</Table.HeaderCell>
								<Table.HeaderCell>Edit</Table.HeaderCell>
							  </Table.Row>
							</Table.Header>

							<Table.Body>
							{this.state.data && Object.keys(this.state.data).length > 0 && <Table.Row>
								<Table.Cell>{this.state.data.name}</Table.Cell>
								<Table.Cell>{this.state.data.document_type}</Table.Cell>
								<Table.Cell>{this.state.data.created_at}</Table.Cell>
								<Table.Cell>{this.state.data.updated_at}</Table.Cell>
								<Table.Cell><Button size='mini' as={Link} to={'/document/'+ this.state.data.id} positive>Edit</Button></Table.Cell>
							</Table.Row>}
							</Table.Body>
						 </Table>
                            
					</Grid.Column>
					</Grid.Row>
			</Grid>
			
			<Grid columns={1} padded="horizontally" className="doc-file">
				<Grid.Row columns={6}>{document_file_html}</Grid.Row>
				<Comment.Group className="wcst-chat">
					<Header as='h3' dividing>Chat</Header>
					<div className='wcst-chat-message' ref='chatoutput'>{chat_data}</div>
					<Form>
					  <Form.TextArea rows="2" name='message' value={this.state.fields.message} placeholder='Message' onChange={this.handleChange} error={errors.has('message')} />
					  {errors.has('message') && <Header size='tiny' className='custom-error' color='red'>
						{errors.first('message')}
                      </Header>}
					  <Button content='Chat' labelPosition='left' icon='edit' primary onClick={this.handleSubmit} />
					</Form>
				  </Comment.Group>
			</Grid>
			</Container>
			</div>
			</div>
        );
    }
}

export default Page;