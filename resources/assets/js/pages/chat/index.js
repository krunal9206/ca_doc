import {connect} from 'react-redux'
import Page from './Page'

const mapStateToProps = (state) => {
    return {
        UserID : state.Auth.user.id,
    }
};

export default connect(mapStateToProps)(Page);