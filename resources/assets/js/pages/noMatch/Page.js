import React from 'react'
import {
    Button,
    Container,
    Grid,
    Header,
    Icon,
    Responsive,
    Segment,
    Step
} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
class Page extends React.Component {
    constructor(props){
        super(props);

    }



    render() {
        return(
		<div>
			<Segment
                    inverted
                    textAlign='center'
                    className='home-header'
                    vertical
                >
                    <Container text>
                        <Responsive minWidth={769}>
                            <Header
                                as="h2"
                                content="Sorry"
                                inverted
                                className="pretitle"
                            />
                        </Responsive>
                        <Header
                            as='h1'
                            content='Page not found!'
                            inverted
                            className="main-heading"
                        />
						<Button color="teal" size='huge' className="free-signup-button">
                            <Link to='/' replace>Back</Link>
                        </Button>
                    </Container>
                </Segment>
				</div>
        );
    }
}

export default Page;