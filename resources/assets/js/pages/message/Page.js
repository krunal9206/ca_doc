import React from 'react'
import {
    Button, Divider, Dimmer, Form, Grid, Header, Icon, Loader, Message, Segment, Container, Modal, Label
} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import PageHeader from '../../common/pageHeader'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import * as ChatService from '../../services/chatService'

class Page extends React.Component {
    constructor(props) {
        super(props);
		
		this.state = {
			chat_count: 0,
		  data: [],
		  pages: null,
		  loading: true,
		};
		this.fetchData = this.fetchData.bind(this);
    }
	
	fetchData(state, instance) {
		this.setState({ loading: true });
		this.props.dispatch(ChatService.unreadData(
			state.pageSize,
			state.page)
			).then((res) => {
				this.setState({
					data: res.data.data,
					pages: res.data.last_page,
					loading: false
				});
			});
  }
  

    render() {
		const { data, pages, loading } = this.state;
		
        return (
            <div>
                <PageHeader heading="Pending Messages"/>
				<div className="course-tour">
				<Container className="step-container">
                            <Grid columns={1} padded="horizontally">
                                <Grid.Row>
                                    <Grid.Column>
										<ReactTable
          columns={[
            {
              Header: "Name",
              accessor: "name",
			  filterable: false,
            },
            {
              Header: "Document Type",
              accessor: "document_type",
			  filterable: false,
            },
			{
              Header: "Date",
              accessor: "created_at",
			  filterable: false,
            },
            {
              Header: "Action",
			  sortable: false,
			  accessor: "id",
			  Cell: ({value}) => (
				<span>
					<Button size='mini' as={Link} to={'/chat/'+ value} color="violet">Chat</Button>
				</span>
			  )
            }
          ]}
          manual
          data={data}
          pages={pages}
          loading={loading}
          onFetchData={this.fetchData}
		  showPageSizeOptions={false}
          defaultPageSize={10}
          className="-striped -highlight"
		  style={{marginTop: '2em'}}
        />
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                    </Container>
				</div>
            </div>
        );
    }
}

export default Page;