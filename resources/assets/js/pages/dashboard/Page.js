import React from 'react'
import {
    Button, Divider, Dimmer, Form, Grid, Header, Icon, Loader, Message, Segment, Container, Modal, Label
} from 'semantic-ui-react'
import {Link} from 'react-router-dom'
import PageHeader from '../../common/pageHeader'
import ReactTable from 'react-table'
import 'react-table/react-table.css'
import * as DocumentService from '../../services/documentService'
import * as ChatService from '../../services/chatService'

class Page extends React.Component {
    constructor(props) {
        super(props);
		
		this.state = {
		  data: [],
		  pages: null,
		  loading: true,
		  open: false,
		  delete_id : null,
		};
		this.fetchData = this.fetchData.bind(this);
		this.modalShow = this.modalShow.bind(this);
		this.deleteData = this.deleteData.bind(this);
		this.close = this.close.bind(this);
    }
	
	fetchData(state, instance) {
		this.setState({ loading: true });
		this.props.dispatch(DocumentService.data(
			state.pageSize,
			state.page,
			state.sorted,
			state.filtered)
			).then((res) => {
				this.setState({
					data: res.data.data,
					pages: res.data.last_page,
					loading: false
				});
			});
  }
  
  modalShow(event) {
	  const value = event.target.value;
	  this.setState({
		open: true,
		delete_id: value
	  });
  }
  
  deleteData(event) {
	  const value = event.target.value;
	  this.setState({
		open: false,
		loading: true,
	  });
		this.props.dispatch(DocumentService.deleteData(this.state.delete_id))
		.then((response) => {
			this.props.dispatch(DocumentService.data(10, 0, [], [])
			).then((res) => {
				this.setState({
					data: res.data.data,
					pages: res.data.last_page,
					loading: false
				});
			});
		})
  }
  
  close(event) {
	  this.setState({ open: false });
  }
  

    render() {
		const { data, pages, loading, open } = this.state;
		
        return (
            <div>
                <PageHeader heading="Dashboard"/>
				<div className="course-tour">
				<Container className="step-container">
                            <Grid columns={1} padded="horizontally">
                                <Grid.Row>
                                    <Grid.Column>
										<Button as={Link} to="/document" positive compact style={{lineHeight: '2em'}}>Add Document</Button>
										<Button as={Link} to="/documents" color='purple' compact style={{lineHeight: '2em'}}>View Documents</Button>
										
										<ReactTable
          columns={[
            {
              Header: "Name",
              accessor: "name",
			  filterable: false,
			  sortable: false,
            },
            {
              Header: "Document Type",
              accessor: "document_type",
			  filterable: true,
            },
			{
              Header: "Date",
              accessor: "created_at",
			  filterable: true,
            },
			{
              Header: "Status",
              accessor: "status",
			  filterable: false,
			  sortable: false,
			  Cell: ({value}) => (
				<span>
				{(value == 'pending') ? <Label size='mini' color='red'>Pending</Label> : 
				(value == 'viewed') ? <Label size='mini' color='yellow'>Viewed</Label> : <Label size='mini' color='green'>Processed</Label>}
				</span>
			  )
            },
            {
              Header: "Action",
			  sortable: false,
			  accessor: "id",
			  Cell: ({value}) => (
				<span>
					<Button size='mini' as={Link} to={'/chat/'+ value} color="violet">View</Button>
					<Button size='mini' as={Link} to={'/document/'+ value} positive>Edit</Button>
					<Button size='mini' onClick={this.modalShow} value={value} negative>Delete</Button>
				</span>
			  )
            }
          ]}
          manual
          data={data}
          pages={pages}
          loading={loading}
          onFetchData={this.fetchData}
		  showPageSizeOptions={false}
          defaultPageSize={10}
          className="-striped -highlight"
		  style={{marginTop: '2em'}}
        />
		
		<Modal size='mini' open={open} onClose={this.close}>
          <Modal.Header>
            Delete Your Account
          </Modal.Header>
          <Modal.Content>
            <p>Are you sure you want to delete your account</p>
          </Modal.Content>
          <Modal.Actions>
            <Button negative onClick={this.close}>
              No
            </Button>
            <Button positive icon='checkmark' labelPosition='right' content='Yes' onClick={this.deleteData} value={this.state.delete_id} />
          </Modal.Actions>
        </Modal>
                                    </Grid.Column>
                                </Grid.Row>
                            </Grid>
                    </Container>
				</div>
            </div>
        );
    }
}

export default Page;