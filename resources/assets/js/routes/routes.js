import Home from '../pages/home'
import Login from '../pages/login'
import Register from '../pages/register'
import ForgotPassword from '../pages/forgotPassword'
import ResetPassword from '../pages/resetPassword'
import Dashboard from '../pages/dashboard'
import Document from '../pages/document'
import Documents from '../pages/documents'
import Chat from '../pages/chat'
import Message from '../pages/message'
import NoMatch from '../pages/noMatch'

const routes = [
    {
        path: '/',
        exact: true,
        auth: false,
        component: Home
    },
    {
        path: '/login',
        exact: true,
        auth: false,
        component: Login
    },
	{
        path: '/document',
        exact: true,
        auth: true,
        component: Document
    },
	{
        path: '/document/:id',
        exact: true,
        auth: true,
        component: Document
    },
	{
        path: '/chat/:id',
        exact: true,
        auth: true,
        component: Chat
    },
    {
        path: '/dashboard',
        exact: true,
        auth: true,
        component: Dashboard
    },
	{
        path: '/documents',
        exact: true,
        auth: true,
        component: Documents
    },
	{
        path: '/message',
        exact: true,
        auth: true,
        component: Message
    },
    {
        path: '',
        exact: true,
        auth: false,
        component: NoMatch
    }
];

export default routes;