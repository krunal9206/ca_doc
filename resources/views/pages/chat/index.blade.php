@extends('layouts.default')
@section('title', 'Messages')

@section('styles')
<link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Unread Messages
        <small>Information about messages</small>
      </h1>
    </section>
    <section class="content">
		<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <div class="box-tools hidden">
                <div class="input-group input-group-sm" style="width: 150px;">
                  <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{!! $error !!}</li>
							@endforeach
						</ul>
					</div>
				@endif
				@if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif
              <table class="table table-bordered table-striped dataTable datatable">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Type</th>
						<th>User</th>
						<th>Created Date</th>
						<th>Updated Date</th>
						<th>Action</th>
					</tr>
				</thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
</div>
@endsection

@section('scripts')
<script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
$(function() {
	 var oTable = $('.datatable').DataTable({
        processing: true,
        serverSide: true,
		pageLength: {{ Config::get('constants.wcst.page_limit') }},
        ajax: "{{ route('message.data') }}",
        columns: [
            {data: 'DT_Row_Index', name: 'id', orderable: false},
            {data: 'name', orderable: false},
            {data: 'document_type', orderable: false},
            {data: 'user.name', orderable: false},
            {data: 'created_at', searchable: false, orderable: false},
            {data: 'updated_at', searchable: false, orderable: false},
			{data: 'action', orderable: false, searchable: false},
        ],
    });
});
</script>
@endsection