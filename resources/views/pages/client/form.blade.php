@extends('layouts.default')
@section('title', $data['page_title'])

@section('content')
<div class="content-wrapper">
	<section class="content-header">
      <h1>{{ $data['page_title'] }}
		<small>{{ $data['page_description'] }}</small>
	  </h1>
    </section>
	<section class="content">
		<div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $data['page_description'] }}</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			
			@if($data['action'] == 'insert')
				{!! Form::open(array('route' => 'client.store', 'method'=>'POST', 'files' => true, 'id' => 'form-user')) !!}
			@else
				{!! Form::model($user, ['method' => 'PATCH','route' => ['client.update', $user->id], 'files' => true, 'id' => 'form-user']) !!}
			@endif
<div class="box-body">
	<div class="form-group required">
		{!! Form::label('name', 'Business Name') !!}
		{!! Form::text('name', null, array('placeholder' => 'Business Name','class' => 'form-control', 'required' => 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('business_type', 'Business Type') !!}
		{!! Form::select('business_type', ['Individual' => 'Individual', 'Partnership Firm' => 'Partnership Firm', 'LLP' => 'LLP', 'Company' => 'Company'] , null, ['class' => 'form-control', 'placeholder' => 'Select Business'])  !!}
	</div>
	<div class="form-group">
		{!! Form::label('name_ppd', 'Proprietor/Partner/Director Name') !!}
		{!! Form::text('name_ppd', null, array('placeholder' => 'Proprietor/Partner/Director Name','class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('pan_no', 'PAN No') !!}
		{!! Form::text('pan_no', null, array('placeholder' => 'PAN No','class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('gst_no', 'GST No') !!}
		{!! Form::text('gst_no', null, array('placeholder' => 'GST No','class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('email', 'Email') !!}
		{!! Form::email('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('mobile', 'Mobile') !!}
		{!! Form::text('mobile', null, array('placeholder' => 'Mobile','class' => 'form-control')) !!}
	</div>
	<div class="form-group" required>
		{!! Form::label('username', 'Username') !!}
		{!! Form::text('username', null, array('placeholder' => 'Username', 'class' => 'form-control', 'required' => 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('password', 'Password') !!}
		@if($data['action'] == 'insert')
			{!! Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control', 'id' => 'password', 'required' => 'required')) !!}
		@else
			{!! Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control', 'id' => 'password')) !!}
		@endif
	</div>
	<div class="form-group">
		{!! Form::label('address', 'Address') !!}
		{!! Form::textarea('address', null, array('placeholder' => 'Address','class' => 'form-control', 'rows' => 3)) !!}
	</div>
	<div class="form-group">
		{!! Form::label('status', 'Status') !!}
		{!! Form::select('status', ['active' => 'Active', 'inactive' => 'Inactive'] , null, ['class' => 'form-control'])  !!}
	</div>
</div>
<!-- /.box-body -->
<div class="box-footer">
	<button type="submit" class="btn btn-primary" id="button-submit">Submit</button>
	<a href="{{ route('user.index') }}" class="btn btn-danger pull-right">Back</a>
</div>

			{!! Form::close() !!}
          </div>
          <!-- /.box -->
          <!-- /.box -->
        </div>
      </div>
    </section>
</div>
@endsection

@section('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
$(function() {
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$('#form-user').submit(function(e) {
		e.preventDefault();
		btn = $('#button-submit');
		$.ajax({
			url: '{{ $data["url"] }}',
			type: 'POST',
			data: $(this).serialize(),
			dataType: 'json',
			beforeSend: function() {
				btn.button('loading');
			},
			success: function(json) {
				$('.error-form-input').remove();
				btn.button('reset');
				if(json.errors){
					for (var input in json.errors) {
						var element = $('#' + input);
						$(element).after('<div class="text-danger error-form-input"><b>' + json['errors'][input] + '</b></div>');
					}
				} else if(json.success){
					location = json['redirect'];
				}
			}
		});
	});
});
</script>
@endsection