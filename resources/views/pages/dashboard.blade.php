@extends('layouts.default')
@section('title', 'Dashboard')
@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>Dashboard</h1>
    </section>
    <section class="content">
	@if(Gate::check('see-only-admin') || Gate::check('see-only-client') || Gate::check('see-only-staff'))
		<div class="row">
        <div class="col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 class="unread_doc">0</h3>
              <p><a href="{{ route('document.index') }}" style="color:white;">Unread Documents</a></p>
            </div>
            <div class="icon">
              <i class="ion ion-document"></i>
            </div>
            <a href="{{ route('document.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <div class="col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3 class="unread_chat">0</h3>
              <p><a href="{{ route('chat.index') }}" style="color:white;">Unread Messages</a></p>
            </div>
            <div class="icon">
              <i class="ion ion-email"></i>
            </div>
            <a href="{{ route('chat.index') }}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
    @endif
	</section>
</div>
@endsection