@extends('layouts.default')
@section('title', $data['page_title'])

@section('content')
<div class="content-wrapper">
	<section class="content-header">
      <h1>{{ $data['page_title'] }}
		<small>{{ $data['page_description'] }}</small>
	  </h1>
    </section>
	<section class="content">
		<div class="row">
        <div class="col-xs-12">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">{{ $data['page_description'] }}</h3>
            </div>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			
			@if($data['action'] == 'insert')
				{!! Form::open(array('route' => 'staff.store', 'method'=>'POST', 'files' => true, 'id' => 'form-user')) !!}
			@else
				{!! Form::model($user, ['method' => 'PATCH','route' => ['staff.update', $user->id], 'files' => true, 'id' => 'form-user']) !!}
			@endif
<div class="box-body">
	<div class="form-group required">
		{!! Form::label('name', 'Name') !!}
		{!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control', 'required' => 'required')) !!}
	</div>
	<div class="form-group required">
		{!! Form::label('username', 'Username') !!}
		{!! Form::text('username', null, array('placeholder' => 'Name','class' => 'form-control', 'required' => 'required')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('email', 'Email') !!}
		{!! Form::email('email', null, array('placeholder' => 'Email','class' => 'form-control')) !!}
	</div>
	<div class="form-group">
		{!! Form::label('password', 'Password') !!}
		@if($data['action'] == 'insert')
			{!! Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control', 'id' => 'password', 'required' => 'required')) !!}
		@else
			{!! Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control', 'id' => 'password')) !!}
		@endif
	</div>
	<div class="row-fluid clearfix">
		<div class="col-sm-12 well well-sm catering-menu-area">
			<p class="lead">Client</p>
			
			@if($user->staffAccess->isNotEmpty())
				@foreach ($user->staffAccess as $i => $staff_access)
					<div class="catering_menu col-sm-12 form-group" data-count="{{ $i }}">
					<div class="col-sm-3 form-group">
						{!! Form::select('client['.$i.'][client_id]', $data['clients'], $staff_access->client_id, array('class' => 'form-control', 'required' => 'required', 'placeholder' => 'Select Client')) !!}
					</div>
					<div class="col-sm-3 form-group">
						@if(explode(',', $staff_access->document_type) === array('Purchase Bill', 'Sales Bill', 'Expense Bill', 'Payment', 'Others'))
							{!! Form::select('client['.$i.'][document_type][]', $data['document_type'], 'All', array('class' => 'form-control', 'required' => 'required', 'multiple' => true)) !!}
						@else
							{!! Form::select('client['.$i.'][document_type][]', $data['document_type'], explode(',', $staff_access->document_type), array('class' => 'form-control', 'required' => 'required', 'multiple' => true)) !!}
						@endif
					</div>
					<div class="col-sm-3 form-group">
						@if ($i == 0)
							<button type="button" class="btn btn-success btn-sm btn_add_menu"><i class="fa fa-plus"></i> Add Client</button>
						@else
							<button type="button" class="btn btn-danger btn-sm btn_remove_catering"><i class="fa fa-minus"></i> Remove</button>
						@endif
					</div>
				</div>
				@endforeach
			@else
			<div class="catering_menu col-sm-12 form-group" data-count="0">
				<div class="col-sm-3 form-group">
					{!! Form::select('client[0][client_id]', $data['clients'], null, array('class' => 'form-control', 'required' => 'required', 'placeholder' => 'Select Client')) !!}
				</div>
				<div class="col-sm-3 form-group">
					{!! Form::select('client[0][document_type][]', $data['document_type'], null, array('class' => 'form-control', 'required' => 'required', 'multiple' => true)) !!}
				</div>
				<div class="col-sm-3 form-group">
					<button type="button" class="btn btn-success btn-sm btn_add_menu"><i class="fa fa-plus"></i> Add Client</button>
				</div>
			</div>
			@endif
		</div>
	</div>
	<div class="form-group">
		{!! Form::label('status', 'Status') !!}
		{!! Form::select('status', ['active' => 'Active', 'inactive' => 'Inactive'] , null, ['class' => 'form-control'])  !!}
	</div>
</div>
<div class="box-footer">
	<button type="submit" class="btn btn-primary" id="button-submit">Submit</button>
	<a href="{{ route('staff.index') }}" class="btn btn-danger pull-right">Back</a>
</div>

			{!! Form::close() !!}
          </div>
        </div>
      </div>
    </section>
</div>
@endsection

@section('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
$(function() {
	var main_count = 1;
	$(document).delegate('.btn_add_menu', 'click', function() {
		
		if($('.catering_menu').length > 1){
			main_count = $('.catering-menu-area').children('.catering_menu').last().data('count') + 1;
		}
		
		var html = '<div class="catering_menu col-sm-12 form-group" data-count="'+main_count+'">';
		
		html += '<div class="col-sm-3 form-group">';
		html += '<select class="form-control" required="required" name="client['+main_count+'][client_id]">';
		html += $('select[name="client[0][client_id]"]').html();
		html += '</select>';
		html += '</div>';
		
		html += '<div class="col-sm-3 form-group">';
		html += '<select class="form-control" required="required" name="client['+main_count+'][document_type][]" multiple>';
		html += $('select[name="client[0][document_type][]"]').html();
		html += '</select>';
		html += '</div>';
		
		html += '<div class="col-sm-3 form-group">';
		html += '<button type="button" class="btn btn-danger btn-sm btn_remove_catering"><i class="fa fa-minus"></i> Remove</button>';
		html += '</div>';
		
		html += '</div>';
		
		$('.catering-menu-area').append(html);
		main_count++;
	});
	
	$(document).delegate('.btn_remove_catering', 'click', function() {
		$(this).closest('.catering_menu').remove();
	});
	
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$('#form-user').submit(function(e) {
		e.preventDefault();
		btn = $('#button-submit');
		$.ajax({
			url: '{{ $data["url"] }}',
			type: 'POST',
			data: $(this).serialize(),
			dataType: 'json',
			beforeSend: function() {
				btn.button('loading');
			},
			success: function(json) {
				$('.error-form-input').remove();
				btn.button('reset');
				if(json.errors){
					for (var input in json.errors) {
						var element = $('#' + input);
						$(element).after('<div class="text-danger error-form-input"><b>' + json['errors'][input] + '</b></div>');
					}
				} else if(json.success){
					location = json['redirect'];
				}
			}
		});
	});
});
</script>
@endsection