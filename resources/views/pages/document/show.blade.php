@extends('layouts.default')
@section('title', $document->name)

@section('content')
<div class="content-wrapper">
	<section class="content-header">
      <h1>{{ $document->name }}</h1>
	  <div class="breadcrumb">
        <a href="{{ route('document.index') }}" class="btn btn-xs btn-danger"><i class="fa fa-arrow-circle-left"></i> Back</a>
      </div>
    </section>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box box-danger">
				<div class="box-body">
					<table class="table table-hover table-bordered">
						<tbody>
						<tr>
							<th>Name</th>
							<th>Type</th>
							<th>User</th>
							<th>Created At</th>
							<th>Updated At</th>
							<th>Processed</th>
						</tr>
						<tr>
							<td>{{ $document->name }}</td>
							<td>{{ $document->document_type }}</td>
							<td>{{ $document->user->name }}</td>
							<td>{{ $document->created_at }}</td>
							<td>{{ $document->updated_at }}</td>
							<td>{{ Form::checkbox('status', 'processed', $document->status == 'processed' ? true : false, ['id' => 'status']) }}</td>
						</tr>
					  </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-lg-12 doc-chat">
		@if($document->files->isNotEmpty())
			<div class="box box-default">
				<div class="box-body ca-docs">
					@foreach ($document->files as $file)
						<div class="col-lg-2 col-xs-6">
							<div class="doc-file">
								<a href="javascript:void(0);" data-href="{{ url('uploads/' . $file->file_name) }}" class="doc-view">
									@if(File::extension($file->file_name) == 'pdf') 
										<img src="{{ url('/dist/img/doc-icon.png') }}" class="img-responsive"/>
									@else
										<img src="{{ url('uploads/' . $file->file_name) }}" class="img-responsive"/>
									@endif
								</a>
								<a href="javascript:void(0);" data-href="{{ url('uploads/' . $file->file_name) }}" class="btn btn-success btn-xs doc-view"><i class="fa fa-thumbs-o-up"></i> View</a>
							</div>
						</div>
					@endforeach
				</div>
			</div>
		@endif
          <div class="box box-warning direct-chat direct-chat-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Chat</h3>
            </div>
			{!! Form::open(array('method'=>'POST', 'id' => 'form-chat')) !!}
				<div class="box-body">
					<div class="direct-chat-messages"></div>
				</div>
				<div class="box-footer">
					<div class="form-group">
						{!! Form::textarea('message', null, array('placeholder' => 'Message','class' => 'form-control', 'required' => 'required', 'rows' => 3, 'id' => 'message')) !!}
					</div>
					<button type="submit" class="btn btn-primary" id="button-submit"><i class="fa fa-envelope-o"></i> Send</button>
					<button type="button" class="btn btn-danger pull-right" id="clear-chat"><i class="fa fa-trash"></i> Clear Chat</button>
				</div>
			{!! Form::close() !!}
            </div>
          </div>
        </div>
	</div>
	</section>
</div>
@endsection

@section('scripts')
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
$(function() {
	 $.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	
	$('#form-chat').submit(function(e) {
		e.preventDefault();
		btn = $('#button-submit');
		$.ajax({
			url: "{{ route('chat.store', ['document' => $document->id]) }}",
			type: 'POST',
			data: $(this).serialize(),
			dataType: 'json',
			beforeSend: function() {
				btn.button('loading');
			},
			success: function(json) {
				btn.button('reset');
				if(json.errors){
					for (var input in json.errors) {
						var element = $('#' + input);
						$(element).after('<div class="text-danger error-form-input"><b>' + json['errors'][input] + '</b></div>');
					}
				} else if(json.success){
					$('#form-chat #message').val('');
				}
			}
		});
	});
	
	$('#clear-chat').on('click', function() {
		btn = $('#clear-chat');
		$.ajax({
			url: "{{ route('chat.destroy', ['document' => $document->id]) }}",
			type: 'DELETE',
			dataType: 'json',
			beforeSend: function() {
				btn.button('loading');
			},
			success: function(json) {
				btn.button('reset');
			}
		});
	});
	
	var oldscrollHeight = $(".direct-chat-messages")[0].scrollHeight;
	
	function chat(){
		$.ajax({
			url: "{{ route('chat.store', ['document' => $document->id]) }}",
			type: 'GET',
			dataType: 'json',
			success: function(json) {
				if(json.success && json.data){
					var data = json.data;
					var html = '';
					for (var key in data) {
						if(data[key]['user_id'] == '{{ Auth::user()->id }}'){
							html += '<div class="direct-chat-msg right">';
						} else {
							html += '<div class="direct-chat-msg">';
						}
						html += '<div class="direct-chat-info clearfix">';
						if(data[key]['user_id'] == '{{ Auth::user()->id }}'){
							html += '<span class="direct-chat-name pull-right">' + data[key]['user']['name'] + '</span>';
							html += '<span class="direct-chat-timestamp pull-left">' + data[key]['created_at'] + '</span>';
						} else {
							html += '<span class="direct-chat-name pull-left">' + data[key]['user']['name'] + '</span>';
							html += '<span class="direct-chat-timestamp pull-right">' + data[key]['created_at'] + '</span>';
						}
						html += '</div>';
						var slug = data[key]['user']['roles'][0]['slug'];
						
						if(slug == 'admin'){
							html += '<img class="direct-chat-img" src="{{ url('/dist/img/ca-chat.png') }}">';
						} else if(slug == 'client'){
							html += '<img class="direct-chat-img" src="{{ url('/dist/img/client-chat.png') }}">';
						} else if(slug == 'user'){
							html += '<img class="direct-chat-img" src="{{ url('/dist/img/user-chat.png') }}">';
						} else if(slug == 'staff') {
							html += '<img class="direct-chat-img" src="{{ url('/dist/img/staff-chat.png') }}">';
						}
						
						html += '<div class="direct-chat-text">' + data[key]['message'] + '</div>';
						html += '</div>';
					}
					$('.direct-chat-messages').html(html);
					
					var newscrollHeight = $(".direct-chat-messages")[0].scrollHeight;
					
					if(newscrollHeight > oldscrollHeight){
						$(".direct-chat-messages").scrollTop($(".direct-chat-messages")[0].scrollHeight);
					}
				}
			},
			complete: function() {
				setTimeout(chat, 2000);
			}
		});
	}
	
	chat();
	
	$('#status').on('change', function() {
		if ($(this).is(':checked')) {
			var status = $(this).val();
		} else {
			var status = 'viewed';
		}
		$.ajax({
			url: "{{ route('document.status', ['document' => $document->id]) }}",
			type: 'POST',
			data: {status: status},
			dataType: 'json',
			success: function(json) {
			}
		});
	});
	
	$('.doc-view').on('click', function() {
		var href = $(this).data('href');
		var html = '<div class="col-lg-12 docs-view"><div class="box box-default">';
		html += '<div class="box-body" style="height: 500px;">';
		html += '<iframe src="' + href + '" style="width: 100%; height: 100%;"></iframe>';
		html += '</div>';
		html += '<div class="box-footer"><button type="button" class="btn btn-danger pull-right" id="close-doc">Close</button></div>';
		html += '</div></div>';
		$('.docs-view').remove();
		$('.doc-chat').before(html);
	});
	
	$(document).on('click', '#close-doc', function() {
		$('.docs-view').remove();
	});
});
</script>
@endsection