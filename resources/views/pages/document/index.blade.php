@extends('layouts.default')
@section('title', 'Documents')

@section('styles')
<link rel="stylesheet" href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Documents
        <small>Information about documents</small>
      </h1>
    </section>
    <section class="content">
		<div class="row">
        <div class="col-xs-12">
			<div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Filter</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-sm-3">
				   {!! Form::select('document_type', $data['document_type'], null, array('class' => 'form-control for-filter', 'id' => 'document_type')) !!}
                </div>
				
				<div class="col-sm-3">
                  {!! Form::select('client_id', $data['clients'], null, array('class' => 'form-control for-filter', 'id' => 'client_id', 'placeholder' => 'Select Client')) !!}
                </div>
				
				<div class="col-sm-3">
                 {!! Form::select('user_id', [null => 'Select User'], null, array('class' => 'form-control for-filter', 'id' => 'user_id')) !!}
                </div>
				
				<div class="col-sm-3">
                 {!! Form::select('status', [null => 'Select Status', 'pending' => 'Pending', 'viewed' => 'Viewed', 'processed' => 'Processed'], null, array('class' => 'form-control for-filter', 'id' => 'status')) !!}
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <div class="box">
            <div class="box-body table-responsive">
				@if (count($errors) > 0)
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<ul>
							@foreach ($errors->all() as $error)
								<li>{!! $error !!}</li>
							@endforeach
						</ul>
					</div>
				@endif
				@if ($message = Session::get('success'))
					<div class="alert alert-success">
						<p>{{ $message }}</p>
					</div>
				@endif
              <table class="table table-bordered table-striped dataTable datatable">
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Type</th>
						<th>User</th>
						@can('see-only-admin')
						<th>Client</th>
						@endcan
						<th>Created Date</th>
						<th>Updated Date</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
</div>
@endsection

@section('scripts')
<script src="{{ asset('/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript">
$(function() {
	 var oTable = $('.datatable').DataTable({
        processing: true,
        serverSide: true,
		language: {
            processing: '<i class="fa fa-cog fa-spin" style="font-size:48px;color:red"></i>'
		},
		bFilter: false,
		pageLength: {{ Config::get('constants.wcst.page_limit') }},
        ajax: {
                url: "{{ route('document.data') }}",
                data: function (d) {
                    d.document_type = $('#document_type').val();
                    d.client_id = $('#client_id').val();
                    d.user_id = $('#user_id').val();
                    d.status = $('#status').val();
			}
        },
		order: [[0, 'desc']],
        columns: [
            {data: 'DT_Row_Index', name: 'id', orderable: false},
            {data: 'name', orderable: false},
            {data: 'document_type', orderable: false},
            {data: 'user.name', orderable: false},
			@can('see-only-admin'){data: 'user.parent_client.name', defaultContent: "-", orderable: false, searchable: false},@endcan
            {data: 'created_at', searchable: false, orderable: false},
            {data: 'updated_at', searchable: false, orderable: false},
			{data: 'status', orderable: false, searchable: false},
			{data: 'action', orderable: false, searchable: false},
        ],
    });
	
	$(".for-filter").on('change',function() {
		oTable.draw();
	});
	
	$('#client_id').on('change', function() {
		$('#user_id').val('');
		var select = $('#user_id');
		select.empty();
		select.append('<option value="">Select User</option>');
		if($(this).val()){
			var url = '{{ url("admin/client") }}/' + $(this).val() + '/users/';
			$.get(url, function(data) {
                $.each(data,function(key, value) {
                    select.append('<option value=' + value.id + '>' + value.name + '</option>');
                });
            });
		}
	});
});
</script>
@endsection