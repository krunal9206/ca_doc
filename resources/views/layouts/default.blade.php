<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
  @yield('styles')
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/skins/skin-purple-light.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/wcst.css') }}">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-purple-light sidebar-mini">
<div class="wrapper">
    @include('includes.header')
    @include('includes.sidebar')
	@yield('content')
	@include('includes.footer')
</div>
<script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/dist/js/adminlte.min.js') }}"></script>
@yield('scripts')

@if(Gate::check('see-only-admin') || Gate::check('see-only-client') || Gate::check('see-only-staff'))
<script type="text/javascript">
function chat_count(){
	$.ajax({
		url: "{{ route('chat.count') }}",
		type: 'GET',
		dataType: 'json',
		success: function(json) {
			if(json.success && json.data){
				data = json.data;
				$('.unread_doc').text(data.unread_doc);
				$('.unread_chat').text(data.unread_chat);
			}
		},
		complete: function() {
			setTimeout(chat_count, 5000);
		}
	});
}	
chat_count();
</script>
@endif
</body>
</html>