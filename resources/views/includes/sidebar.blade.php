<aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ url('/dist/img/avatar5.png') }}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="{{ Request::is('admin/dashboard') ? 'active' : '' }}"><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
		@can('see-only-admin')
		<li class="{{ Request::is('admin/client') ? 'active' : '' }}">
          <a href="{{ route('client.index') }}"><i class="fa fa-cc"></i> 
			<span>Client</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
			</span>
          </a>
        </li>
		
		<li class="{{ Request::is('admin/staff') ? 'active' : '' }}">
          <a href="{{ route('staff.index') }}"><i class="fa fa-skype"></i> 
			<span>Staff</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
			</span>
          </a>
        </li>
		@endcan
		@if(Gate::check('see-only-super') || Gate::check('see-only-admin'))
		<li class="{{ Request::is('admin/user') ? 'active' : '' }}">
          <a href="{{ route('user.index') }}"><i class="fa fa-user"></i> 
		  @can('see-only-super')
			<span>CA</span>
		  @endcan
		  @can('see-only-admin')
			<span>User</span>
		  @endcan
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
          </a>
        </li>
		@endif
		
		@if(Gate::check('see-only-admin') || Gate::check('see-only-client') || Gate::check('see-only-staff'))
		<li class="{{ Request::is('admin/document') ? 'active' : '' }}">
          <a href="{{ route('document.index') }}"><i class="fa fa-file"></i>
			<span>Documents</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red unread_doc">0</small>
            </span>
          </a>
        </li>
		<li class="{{ Request::is('admin/chat') ? 'active' : '' }}">
          <a href="{{ route('chat.index') }}"><i class="fa fa-envelope"></i>
			<span>Message</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red unread_chat">0</small>
            </span>
          </a>
        </li>
		@endif
      </ul>
    </section>
</aside>