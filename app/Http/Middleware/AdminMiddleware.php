<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (Auth::check())
        {
            if(Auth::user()->inRole('user'))
			{
				Auth::logout();
				$request->session()->flash('message.level', 'danger');
				$request->session()->flash('message.content', 'You do not have access this part.');
				return redirect()->route('login');
			}
        }
		return $next($request);
    }
}
