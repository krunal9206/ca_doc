<?php 
namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Document;
use App\DocumentFiles;
use App\User;
use Validator;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;

class DocumentController extends Controller {
	
	public function __construct() {
		$this->middleware('jwt_auth');
    }
	
	public function data(Request $request)
    {
		$user = JWTAuth::parseToken()->authenticate();
		
		$page_size = $request->query('page_size');
		$sorted = json_decode($request->query('sorted'));
		$filtered = json_decode($request->query('filtered'));
		
		$query = Document::where('user_id', $user->id);
		
		if(!empty($filtered)){
			foreach($filtered as $filter){
				if($filter->id == 'created_at') {
					$query->whereRaw("DATE_FORMAT(created_at,'%d-%m-%Y') like ?", ["%".$filter->value."%"]);
				} else {
					$query->where($filter->id, 'like', $filter->value . '%');
				}
			}
		}
		
		if(!empty($sorted)){
			if($sorted[0]->desc){
				$query->orderBy($sorted[0]->id, 'desc');
			} else {
				$query->orderBy($sorted[0]->id, 'asc');
			}
		}
		
		$query->orderByRaw("FIELD(status , 'pending', 'viewed', 'processed') ASC");
		$query->orderBy('created_at', 'DESC');
		
		$documents = $query->paginate($page_size);

		return response()->json([
            'data'  => $documents,
			'message'	=> 'Successfully get data.'
        ], 200);
	}
	
	public function featchData($id)
    {
		$user = JWTAuth::parseToken()->authenticate();
		$document = Document::with('files')->where([
					['user_id', '=', $user->id],
					['id', '=', $id],
				])->first();
				
		if($document){
			return response()->json([
				'data'  => $document,
				'message'	=> 'Successfully get data.'
			], 200);
		}
		
		return response()->json([
			"error" => "invalid_data",
			"message" => "Not Found."
		], 422);
	}
	
	public function store(Request $request)
    {
		$user = JWTAuth::parseToken()->authenticate();
		
		$validator = Validator::make($request->all(), [
            'upload_type' => 'required|in:Single Upload,Bulk Upload',
            'document_type' => 'required|in:Purchase Bill,Sales Bill,Expense Bill,Deposit Slip,Payment,Others',
			'document_file.*' => 'required|mimes:jpg,jpeg,png,pdf'
        ]);
		
		if ($validator->fails()) {
            return response()->json([
				"error" => "invalid_input",
				"message" => "Please enter fields."
			], 422);
        }
		
		if($request->input('upload_type') == 'Single Upload'){
			$document = Document::create([
				'user_id' => $user->id,
				'document_type' => $request->input('document_type'),
				'upload_type' => $request->input('upload_type'),
			]);
			
			$document->update(['name' => 'Document_'.$document->id]);
			
			$file_path = $this->filePath($document->user_id);
			foreach ($request->document_file as $document_file) {
				$file_name = $file_path . $document_file->hashName();
				//$file_name = $document_file->store('documents');
				
				Storage::disk('public_uploads')->put($file_name, file_get_contents($document_file));
				$document->files()->create([
					'ori_name' => $document_file->getClientOriginalName(),
					'file_name' => $file_name
				]);
			}
		} else {
			foreach ($request->document_file as $document_file) {
				$document = Document::create([
					'user_id' => $user->id,
					'document_type' => $request->input('document_type'),
					'upload_type' => $request->input('upload_type'),
				]);
				
				$document->update(['name' => 'Document_'.$document->id]);
				
				$file_path = $this->filePath($document->user_id);
				$file_name = $file_path . $document_file->hashName();
				
				Storage::disk('public_uploads')->put($file_name, file_get_contents($document_file));
				$document->files()->create([
					'ori_name' => $document_file->getClientOriginalName(),
					'file_name' => $file_name
				]);
			}
		}
		
		return response()->json([
			'message'	=> 'Successfully added.'
        ], 200);
	}
	
	public function update(Request $request, Document $document)
    {
		$validator = Validator::make($request->all(), [
            'document_type' => 'required|in:Purchase Bill,Sales Bill,Expense Bill,Deposit Slip,Payment,Others',
			'document_file.*' => 'sometimes|required|mimes:jpg,jpeg,png,pdf'
        ]);
		
		if ($validator->fails()) {
            return response()->json([
				"error" => "invalid_input",
				"message" => "Please enter fields."
			], 422);
        }
		
		$data = $request->only('document_type');
		
		if($request->has('document_file')){
			$file_path = $this->filePath($document->user_id);
			foreach ($request->file('document_file') as $document_file) {
				$file_name = $file_path . $document_file->hashName();
				//$file_name = $document_file->store('documents');
				
				Storage::disk('public_uploads')->put($file_name, file_get_contents($document_file));
				$document->files()->create([
					'ori_name' => $document_file->getClientOriginalName(),
					'file_name' => $file_name
				]);
			}
		}
		
		$data['status'] = 'pending';
		
		$document->fill($data)->save();
		
		return response()->json([
            'document'  => $document,
			'message'	=> 'Successfully added.'
        ], 200);
	}
	
	public function filePath($user_id)
	{
		$user = User::select('parent_user', 'parent_client', 'username')->where('id', $user_id)->first();
		$client = User::select('username')->where('id', $user->parent_user)->first();
		$ca = User::select('username')->where('id', $user->parent_client)->first();
		return $ca->username . '/' . $client->username . '/' . $user->username . '/';
	}
	
	public function destroy($id)
    {
		$user = JWTAuth::parseToken()->authenticate();
		$document = Document::with('files')->where([
					['user_id', '=', $user->id],
					['id', '=', $id],
				])->first();
		
		if($document){
			
			if($document->files->isNotEmpty()){
				foreach($document->files as $file){
					//Storage::delete($file->file_name);
					Storage::disk('public_uploads')->delete($file->file_name);
					DocumentFiles::find($file->id)->delete();
				}
			}
			$document->chats()->delete();
			$document->delete();
			
			return response()->json([
				'document'  => null,
				'message'	=> 'Successfully deleted.'
			], 200);
		}
		
		return response()->json([
			"error" => "not_found",
			"message" => "Not Found."
		], 422);
	}
	
	public function deleteFile($document, $file)
    {
		$user = JWTAuth::parseToken()->authenticate();
		$document = Document::where([
					['user_id', '=', $user->id],
					['id', '=', $document],
				])->first();
		
		if($document){
			$file = DocumentFiles::find($file);
			
			if($file) {
				//Storage::delete($file->file_name);
				Storage::disk('public_uploads')->delete($file->file_name);
				$file->delete();
				
				return response()->json([
					'document'  => null,
					'message'	=> 'Successfully deleted.'
				], 200);
			}
		}
		
		return response()->json([
			"error" => "not_found",
			"message" => "Not Found."
		], 422);
	}
	
	public function featchDocuments(Request $request)
    {
		$user = JWTAuth::parseToken()->authenticate();
		$user_id = $user->id;
		
		$page_size = $request->query('page_size');
		$document_type = $request->query('document_type');
		$query = DocumentFiles::whereHas('document', function($q) use ($user_id){
			$q->where('user_id', $user_id);
		});
		if($document_type){
			$query->whereHas('document', function($q) use ($document_type){
				$q->where('document_type', $document_type);
			});
		}
		$query->orderBy('id', 'DESC');
		
		$files = $query->paginate($page_size);

		return response()->json([
            'data'  => $files,
			'message'	=> 'Successfully get data.'
        ], 200);
	}
}