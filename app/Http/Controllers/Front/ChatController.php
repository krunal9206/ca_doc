<?php 
namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Document;
use App\Chat;
use Validator;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;

class ChatController extends Controller {
	
	public function __construct() {
		$this->middleware('jwt_auth');
    }
	
	public function featchData($document)
    {
		$user = JWTAuth::parseToken()->authenticate();
		$document_count = Document::where([
					['user_id', '=', $user->id],
					['id', '=', $document],
				])->count();
				
		if($document_count){
			Chat::where([
					['document_id', '=', $document],
					['user_id', '!=',$user->id]
			])->update(['status' => 'read']);
			
			$data = Chat::with(array(
				'user' => function($q){
					$q->select('id', 'name');
				},
			))->where('document_id', $document)->get();
			
			return response()->json([
				'data'  => $data,
				'message'	=> 'Successfully get data.'
			], 200);
		}
		
		return response()->json([
			"error" => "invalid_data",
			"message" => "Not Found."
		], 422);
	}
	
	public function store(Request $request, $document)
    {
		$validator = Validator::make($request->all(), [
            'message' => 'required',
        ]);
		
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }
		
		$user = JWTAuth::parseToken()->authenticate();
		
		$document_count = Document::where([
					['user_id', '=', $user->id],
					['id', '=', $document],
				])->count();
		
		if($document_count) {
			$caht = Chat::create([
				'document_id' => $document,
				'user_id' => $user->id,
				'message' => $request->input('message'),
			]);
			
			return response()->json([
				'caht'  => $caht,
				'message'	=> 'Successfully added.'
			], 200);
		}
		
		return response()->json([
			"error" => "not_found",
			"message" => "Not Found."
		], 422);
	}
	
	public function chatCount()
    {
		$user = JWTAuth::parseToken()->authenticate();
		$user_id = $user->id;
		
		$data = Chat::whereHas('document', function($q) use ($user_id){
					$q->where('user_id', $user_id);
				})->where([
					['status', '=', 'unread'],
					['user_id', '!=', $user_id],
				])->count();
		
		return response()->json([
			'data'  => $data,
			'message'	=> 'Successfully get data.'
		], 200);
	}
	
	public function unreadMessage(Request $request)
    {
		$user = JWTAuth::parseToken()->authenticate();
		$user_id = $user->id;
		
		$page_size = $request->query('page_size');
		$query = Document::whereHas('chats', function($q) use ($user_id){
					$q->where([
						['status', '=', 'unread'],
						['user_id', '!=', $user_id],
					]);
				})->where('user_id', $user_id);
		
		$documents = $query->paginate($page_size);

		return response()->json([
            'data'  => $documents,
			'message'	=> 'Successfully get data.'
        ], 200);
	}
}