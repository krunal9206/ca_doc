<?php 
namespace App\Http\Controllers\Pages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Document;
use App\Chat;
use Validator;
use Gate;
use DataTables;

class ChatController extends Controller {
	
	public function __construct() {
        $this->middleware('auth');
		$this->middleware('admin');
    }
	
	public function index()
	{
        return view('pages.chat.index');
    }
	
	public function getMessageData()
	{
		$user = Auth::user();
		$user_id = $user->id;
		
		$query = Document::with(array(
			'user' => function($q){
				$q->select('id', 'name');
			},
		));
		
		if($user->inRole('admin')) {
			$query->whereHas('chats', function($q) use ($user_id){
				$q->whereHas('user', function($q) use ($user_id){
					$q->where('parent_client', $user_id);
				})->where('status', 'unread');
			});
		} else if($user->inRole('client')){
			$query->whereHas('chats', function($q) use ($user_id){
				$q->whereHas('user', function($q) use ($user_id){
					$q->where('parent_user', $user_id);
				})->where('status', 'unread');
			});
		} else if($user->inRole('staff')){
			foreach ($user->staffAccess as $staff_access){
				$client_id = $staff_access->client_id;
				$document_type = $staff_access->document_type;
				$query->where(function ($query) use ($client_id, $document_type) {
					$query->whereHas('chats', function($q) use ($client_id){
						$q->whereHas('user', function($q) use ($client_id){
							$q->where('parent_user', $client_id);
						})->where('status', 'unread');
					})->whereIn('document_type', explode(',', $document_type));
				});
			}
		}
		/* $query->whereHas('chats', function($q){
			$q->where('status', 'unread');
		}); */

		return DataTables::eloquent($query)
		->addIndexColumn()
		->editColumn('status', function($document){
			if($document->status == 'unread'){
				return '<span class="text-bold" style="color: red;">Need to check</span>';
			}
			return '<span class="text-bold text-success">Reviewed</span>';
		})
		->addColumn('action', function($document){
			$buttons = '<a href="'.route('document.show', $document->id).'" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-eye-open"></i> Chat</a>';
			return $buttons;
		})
        ->rawColumns(['action', 'status'])
		->toJson();
    }
	
	public function store(Request $request, Document $document)
    {
		$validator = Validator::make($request->all(), [
            'message' => 'required',
        ]);
		
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }
		
		$login_user = Auth::user();
		if($document) {
			Chat::create([
				'document_id' => $document->id,
				'user_id' => Auth::user()->id,
				'message' => $request->input('message'),
			]);
			return response()->json(['success' => true]);
		}
		
		return response()->json(['success' => false]);
	}
	
	public function getData(Document $document)
	{
		if($document) {
			$user = Auth::user();
			$user_id = $user->id;	
			if($user->inRole('admin')) {
				Chat::whereHas('user', function($q) use ($user_id){
					$q->where('parent_client', $user_id);
				})->where([
					['document_id', '=', $document->id],
					//['user_id', '!=', Auth::user()->id]
				])->update(['status' => 'read']);
			} else if($user->inRole('client')){
				Chat::whereHas('user', function($q) use ($user_id){
					$q->where('parent_user', $user_id);
				})->where([
					['document_id', '=', $document->id],
					//['user_id', '!=', Auth::user()->id]
				])->update(['status' => 'read']);
			} else if($user->inRole('staff')){
				Chat::where([
					['document_id', '=', $document->id],
					['user_id', '=', $document->user_id]
				])->update(['status' => 'read']);
			}
			
			$data = Chat::with(array(
				'user' => function($q){
					$q->select('id', 'name');
				},
				'user.roles' => function($q){
					$q->select('id', 'slug');
				}
			))->where('document_id', $document->id)->get();
			return response()->json(['success' => true, 'data' => $data]);
		}
		return response()->json(['success' => false]);
    }
	
	public function destroy(Document $document)
	{
		if($document) {
			Chat::where('document_id', $document->id)->delete();
			return response()->json(['success' => true]);
		}
		return response()->json(['success' => false]);
    }
	
	public function chatCount(){
		$user = Auth::user();
		$user_id = $user->id;
		
		if($user->inRole('admin')) {
			$data['unread_doc'] = Document::whereHas('user', function($q) use ($user_id){
				$q->where('parent_client', $user_id);
			})->where('status', 'pending')->count();
		
			$data['unread_chat'] = Chat::whereHas('user', function($q) use ($user_id){
				$q->where('parent_client', $user_id);
			})->where('status', 'unread')->count();
			
		} else if($user->inRole('client')){
			$data['unread_doc'] = Document::whereHas('user', function($q) use ($user_id){
				$q->where('parent_user', $user_id);
			})->where('status', 'pending')->count();
		
			$data['unread_chat'] = Chat::whereHas('user', function($q) use ($user_id){
				$q->where('parent_user', $user_id);
			})->where('status', 'unread')->count();
			
		} else if($user->inRole('staff')){
			
			$query_doc = new Document;
			$query_chat = new Chat;
			
			foreach ($user->staffAccess as $staff_access){
				$client_id = $staff_access->client_id;
				$document_type = $staff_access->document_type;
				$query_doc = $query_doc->where(function ($query) use ($client_id, $document_type) {
					$query->whereHas('user', function($q) use ($client_id, $document_type){
						$q->where('parent_user', $client_id);
					})->whereIn('document_type', explode(',', $document_type));
				});
				$query_chat = $query_chat->where(function ($query) use ($client_id, $document_type) {
					$query->whereHas('document', function($q) use ($document_type){
						$q->whereIn('document_type', explode(',', $document_type));
					})->whereHas('user', function($q) use ($client_id){
							$q->where('parent_user', $client_id);
					});
				});
			}
			
			$data['unread_doc'] = $query_doc->where('status', 'pending')->count();
			$data['unread_chat'] = $query_chat->where('status', 'unread')->count();
		}
		
		return response()->json(['success' => true, 'data' => $data]);
	}
}