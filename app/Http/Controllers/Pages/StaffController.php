<?php 
namespace App\Http\Controllers\Pages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Config;
use DataTables;
use Validator;
use Carbon\Carbon;
use Gate;

class StaffController extends Controller {
	
	public function __construct() {
        $this->middleware('auth');
		$this->middleware('admin');
    }
	
    public function index()
	{
        return view('pages.staff.index');
    }
	
	public function getData()
	{
		$user = Auth::user();
		if($user->inRole('super')){
			$query = User::whereHas('roles', function($q){
				$q->where('slug', 'admin');
			});
		} else if($user->inRole('admin')) {
			$user_id = $user->id;
			$query = User::whereHas('roles', function($q){
				$q->where('slug', 'staff');
			})->where('parent_user', $user_id);
		}

		return DataTables::eloquent($query)
		->addIndexColumn()
		->addColumn('action', function($user){
			$buttons = '<a href="'.route('staff.edit', $user->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
			return $buttons;
		})
		->editColumn('parent_client.name', function($user){
			if($user->parent_client){
				return $user->parentClient->name;
			}
			return "-";
		})
		->editColumn('created_at', function($user){
			return $user->created_at ? with(new Carbon($user->created_at))->format('d/m/Y') : '';
		})
		->filterColumn('created_at', function ($query, $keyword) {
             $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
        })
        ->rawColumns(['checkbox', 'action'])
		->toJson();
    }
	
	public function create()
    {
		$data['page_title'] = 'Add Staff';
		$data['page_description'] = 'Information about staff';
		$data['action'] = 'insert';
		$data['url'] = route('staff.store');
		
		$user = Auth::user();
		if($user->inRole('admin')) {
			$data['clients'] = User::whereHas('roles', function($q){
				$q->where('slug', 'client');
			})->where('parent_user',$user->id)->pluck('name', 'id');
			
			$data['document_type'] = [
				'All' => 'All Documents',
				'Purchase Bill'	=> 'Purchase Bill',
				'Sales Bill'	=> 'Sales Bill',
				'Expense Bill'	=> 'Expense Bill',
				'Payment'		=> 'Payment',
				'Others'		=> 'Others',
			];
		}
		
        return view('pages.staff.form', compact('data'));
    }
	
	public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users|max:20',
            'email' => 'nullable|email|unique:users',
            'password' => 'required|min:6',
			'client.*.client_id' => 'required',
			'client.*.document_type.*' => 'required',
        ]);
		
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }
		
		$login_user = Auth::user();
		$parent_user = $login_user->id;
		
		$user = User::create([
			'name' => $request->input('name'),
			'username' => $request->input('username'),
			'email' => $request->input('email'),
			'password' => bcrypt($request->input('password')),
			'status' => $request->input('status'),
			'parent_user' => $parent_user,
			'parent_client' => 0,
		]);
		
		$clients = $request->input('client');
		if(!empty($clients)){
			foreach($clients as $client){
				if(in_array('All', $client['document_type'])){
					$document_type = implode(',', array('Purchase Bill', 'Sales Bill', 'Expense Bill', 'Payment', 'Others'));
				} else {
					$document_type = implode(',', $client['document_type']);
				}
				$user->staffAccess()->create([
					'client_id' => $client['client_id'],
					'document_type' => $document_type
				]);
			}
		}
		
		$user->roles()->attach(5);
		
		$request->session()->flash('success','Staff user created successfully');
		return response()->json(['success' => true, 'redirect' => route('staff.index')]);
	}
	
	public function edit(User $staff)
	{
		$user = $staff;
		if (Gate::denies('user-access', $user)) {
			abort(404);
		}
		
		$data['page_title'] = 'Edit User';
		$data['page_description'] = 'Information about user';
		$data['action'] = 'edit';
		
		$login_user = Auth::user();
		if($login_user->inRole('admin')) {
			$data['clients'] = User::whereHas('roles', function($q){
				$q->where('slug', 'client');
			})->where('parent_user', $login_user->id)->pluck('name', 'id');
			
			$data['document_type'] = [
				'All' => 'All Documents',
				'Purchase Bill'	=> 'Purchase Bill',
				'Sales Bill'	=> 'Sales Bill',
				'Expense Bill'	=> 'Expense Bill',
				'Payment'		=> 'Payment',
				'Others'		=> 'Others',
			];
		}
		
		$data['url'] = route('staff.update', $user->id);
		
        return view('pages.staff.form', compact('user', 'data'));
    }
	
	public function update(Request $request, User $staff) 
	{
		$user = $staff;
		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|max:20|unique:users,username,'.$user->id,
            'email' => 'nullable|unique:users,email,'.$user->id,
            'password' => 'min:6|nullable',
			'client.*.client_id' => 'required',
			'client.*.document_type.*' => 'required',
        ]);
		
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }
		
		if ($request->filled('password')) {
			$data = $request->except('client');
			$data['password'] = bcrypt($request->input('password'));
		} else {
			$data = $request->except('password', 'client');
		}
		
        $user->update($data);
		
		$clients = $request->input('client');
		if(!empty($clients)){
			$user->staffAccess()->delete();
			foreach($clients as $client){
				if(in_array('All', $client['document_type'])){
					$document_type = implode(',', array('Purchase Bill', 'Sales Bill', 'Expense Bill', 'Payment', 'Others'));
				} else {
					$document_type = implode(',', $client['document_type']);
				}
				$user->staffAccess()->create([
					'client_id' => $client['client_id'],
					'document_type' => $document_type
				]);
			}
		}
		
		$request->session()->flash('success','User updated successfully');
		
		return response()->json(['success' => true, 'redirect' => route('staff.index')]);
    }
}