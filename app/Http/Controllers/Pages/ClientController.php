<?php 
namespace App\Http\Controllers\Pages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Config;
use DataTables;
use Validator;
use Carbon\Carbon;
use Gate;

class ClientController extends Controller {
	
	public function __construct() {
        $this->middleware('auth');
		$this->middleware('admin');
    }
	
    public function index()
	{
        return view('pages.client.index');
    }
	
	public function getData()
	{
		$user = Auth::user();
		if($user->inRole('super')){
			$query = User::whereHas('roles', function($q){
				$q->where('slug', 'admin');
			});
		} else if($user->inRole('admin')) {
			$user_id = $user->id;
			$query = User::whereHas('roles', function($q){
				$q->where('slug', 'client');
			})->where('parent_user', $user_id);
		}

		return DataTables::eloquent($query)
		->addIndexColumn()
		->addColumn('action', function($user){
			$buttons = '<a href="'.route('client.edit', $user->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
			return $buttons;
		})
		->editColumn('parent_client.name', function($user){
			if($user->parent_client){
				return $user->parentClient->name;
			}
			return "-";
		})
		->editColumn('created_at', function($user){
			return $user->created_at ? with(new Carbon($user->created_at))->format('d/m/Y') : '';
		})
		->filterColumn('created_at', function ($query, $keyword) {
             $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
        })
        ->rawColumns(['checkbox', 'action'])
		->toJson();
    }
	
	public function create()
    {
		$data['page_title'] = 'Add Client';
		$data['page_description'] = 'Information about client';
		$data['action'] = 'insert';
		$data['url'] = route('client.store');
		
        return view('pages.client.form', compact('data'));
    }
	
	public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users|max:20',
            'email' => 'nullable|email|unique:users',
            'password' => 'required|min:6',
        ]);
		
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }
		
		$login_user = Auth::user();
		$parent_client = 0;
		$parent_user = $login_user->id;
		
		$user = User::create([
			'name' => $request->input('name'),
			'username' => $request->input('username'),
			'email' => $request->input('email'),
			'password' => bcrypt($request->input('password')),
			'status' => $request->input('status'),
			'parent_user' => $parent_user,
			'parent_client' => $parent_client,
			'business_type' => $request->input('business_type'),
			'name_ppd' => $request->input('name_ppd'),
			'pan_no' => $request->input('pan_no'),
			'gst_no' => $request->input('gst_no'),
			'mobile' => $request->input('mobile'),
			'address' => $request->input('address'),
		]);
		
		$user->roles()->attach(4);
		
		$request->session()->flash('success','User created successfully');
		return response()->json(['success' => true, 'redirect' => route('client.index')]);
	}
	
	public function edit(User $client)
	{
		$user = $client;
		if (Gate::denies('user-access', $user)) {
			abort(404);
		}
		
		$data['page_title'] = 'Edit User';
		$data['page_description'] = 'Information about user';
		$data['action'] = 'edit';
		
		$data['url'] = route('client.update', $user->id);
		
        return view('pages.client.form', compact('user', 'data'));
    }
	
	public function update(Request $request, User $client) 
	{
		$user = $client;
		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|max:20|unique:users,username,'.$user->id,
            'email' => 'nullable|unique:users,email,'.$user->id,
            'password' => 'min:6|nullable',
        ]);
		
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }
		
		if ($request->filled('password')) {
			$data = $request->all();
			$data['password'] = bcrypt($request->input('password'));
		} else {
			$data = $request->except('password');
		}
		
        $user->update($data);
		
		$request->session()->flash('success','User updated successfully');
		
		return response()->json(['success' => true, 'redirect' => route('client.index')]);
    }
	
	public function getUsers($client_id) 
	{
		return User::where('parent_user', $client_id)->select('id', 'name')->get();
    }
}