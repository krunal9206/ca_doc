<?php 
namespace App\Http\Controllers\Pages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Config;
use DataTables;
use Validator;
use Carbon\Carbon;
use Gate;

class UserController extends Controller {
	
	public function __construct() {
        $this->middleware('auth');
		$this->middleware('admin');
    }
	
    public function index()
	{
        return view('pages.user.index');
    }
	
	public function getData()
	{
		$user = Auth::user();
		if($user->inRole('super')){
			$query = User::whereHas('roles', function($q){
				$q->where('slug', 'admin');
			});
		} else if($user->inRole('admin')) {
			$user_id = $user->id;
			$query = User::with(array(
				'parentClient' => function($query){
					$query->select('id', 'name');
				}))->where('parent_client', $user_id);
		}

		return DataTables::eloquent($query)
		->addIndexColumn()
		->addColumn('action', function($user){
			$buttons = '<a href="'.route('user.edit', $user->id).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
			return $buttons;
		})
		->editColumn('parent_client.name', function($user){
			if($user->parent_client){
				return $user->parentClient->name;
			}
			return "-";
		})
		->editColumn('created_at', function($user){
			return $user->created_at ? with(new Carbon($user->created_at))->format('d/m/Y') : '';
		})
		->filterColumn('created_at', function ($query, $keyword) {
             $query->whereRaw("DATE_FORMAT(created_at,'%d/%m/%Y') like ?", ["%$keyword%"]);
        })
        ->rawColumns(['checkbox', 'action'])
		->toJson();
    }
	
	public function create()
    {
		$data['page_title'] = 'Add User';
		$data['page_description'] = 'Information about user';
		$data['action'] = 'insert';
		$data['url'] = route('user.store');
		$user = Auth::user();
		if($user->inRole('admin')) {
			$data['clients'] = User::whereHas('roles', function($q){
				$q->where('slug', 'client');
			})->where('parent_user',$user->id)->pluck('name', 'id');
		}
		
        return view('pages.user.form', compact('data'));
    }
	
	public function store(Request $request)
    {
		$validator = Validator::make($request->all(), [
            'name' => 'required',
            'user_type' => 'sometimes|required',
            'client_id' => 'required_if:user_type,==,user',
            'username' => 'required|unique:users|max:20',
            'email' => 'nullable|email|unique:users',
            'password' => 'required|min:6',
        ]);
		
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }
		
		$login_user = Auth::user();
		$parent_client = 0;
		if($login_user->inRole('super')){
			$parent_user = $login_user->id;
		} else if($login_user->inRole('admin')){
			if($request->input('user_type') == 'client'){
				$parent_user = $login_user->id;
			} else {
				$parent_user = $request->input('client_id');
				$parent_client = $login_user->id;
			}
		}
		
		$user = User::create([
			'name' => $request->input('name'),
			'username' => $request->input('username'),
			'email' => $request->input('email'),
			'password' => bcrypt($request->input('password')),
			'status' => $request->input('status'),
			'parent_user' => $parent_user,
			'parent_client' => $parent_client,
		]);
		
		if($login_user->inRole('super')){
			$user->roles()->attach(2);
		} else if($login_user->inRole('admin')){
			if($request->input('user_type') == 'client'){
				$user->roles()->attach(4);
			} else {
				$user->roles()->attach(3);
			}
		}
		$request->session()->flash('success','User created successfully');
		return response()->json(['success' => true, 'redirect' => route('user.index')]);
	}
	
	public function edit(User $user)
	{
		if (Gate::denies('user-access', $user)) {
			abort(404);
		}
		
		$data['page_title'] = 'Edit User';
		$data['page_description'] = 'Information about user';
		$data['action'] = 'edit';
		
		$data['url'] = route('user.update', $user->id);
		
		$login_user = Auth::user();
		if($login_user->inRole('admin')) {
			$data['clients'] = User::where('parent_user', Auth::user()->id)->pluck('name', 'id');
			$user->user_type = $user->roles[0]->slug;
			if($user->user_type == 'user'){
				$user->client_id = $user->parent_user;
			}
		}
		
        return view('pages.user.form', compact('user', 'data'));
    }
	
	public function update(Request $request, User $user) 
	{
		$validator = Validator::make($request->all(), [
            'name' => 'required',
			'user_type' => 'sometimes|required',
            'client_id' => 'required_if:user_type,==,user',
			'username' => 'required|max:20|unique:users,username,'.$user->id,
            'email' => 'nullable|unique:users,email,'.$user->id,
            'password' => 'min:6|nullable',
        ]);
		
		if ($validator->fails()) {
            return response()->json(['errors' => $validator->messages()]);
        }
		
		if ($request->filled('password')) {
			$data = $request->all();
			$data['password'] = bcrypt($request->input('password'));
		} else {
			$data = $request->except('password');
		}
		
		$login_user = Auth::user();
		if($login_user->inRole('admin')){
			$parent_client = 0;
			if($request->input('user_type') == 'client'){
				$data['parent_user'] = $login_user->id;
			} else {
				$data['parent_user'] = $request->input('client_id');
				$data['parent_client'] = $login_user->id;
			}
		}
        $user->update($data);
		
		$request->session()->flash('success','User updated successfully');
		
		return response()->json(['success' => true, 'redirect' => route('user.index')]);
    }
}