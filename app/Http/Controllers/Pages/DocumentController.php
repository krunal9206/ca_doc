<?php 
namespace App\Http\Controllers\Pages;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Document;
use DataTables;
use Validator;
use Carbon\Carbon;
use Gate;

class DocumentController extends Controller {
	
	public function __construct() {
        $this->middleware('auth');
		$this->middleware('admin');
    }
	
    public function index()
	{
		$data['document_type'] = [
			null => 'Select Document Type',
			'Purchase Bill'	=> 'Purchase Bill',
			'Sales Bill'	=> 'Sales Bill',
			'Expense Bill'	=> 'Expense Bill',
			'Payment'		=> 'Payment',
			'Others'		=> 'Others',
		];
		
		$data['clients'] = User::where('parent_user', Auth::user()->id)->pluck('name', 'id');
        return view('pages.document.index', compact('data'));
    }
	
	public function getData(Request $request)
	{
		$user = Auth::user();
		$user_id = $user->id;
		if($user->inRole('admin')) {
			$query = Document::with(array(
				'user.parentClient' => function($q){
					$q->select('id', 'name');
				}
			));
			$query->whereHas('user', function($q) use ($user_id){
				$q->where('parent_client', $user_id);
			});
		} else if($user->inRole('client')){
			$query = Document::with(array(
				'user' => function($q){
					$q->select('id', 'name');
				}
			));
			$query->whereHas('user', function($q) use ($user_id){
				$q->where('parent_user', $user_id);
			});
		} else if($user->inRole('staff')){
			$query = Document::with(array(
				'user' => function($q){
					$q->select('id', 'name');
				}
			));
			
			foreach ($user->staffAccess as $staff_access){
				$client_id = $staff_access->client_id;
				$document_type = $staff_access->document_type;
				$query->where(function ($query) use ($client_id, $document_type) {
					$query->whereHas('user', function($q) use ($client_id, $document_type){
						$q->where('parent_user', $client_id);
					})->whereIn('document_type', explode(',', $document_type));
				});
			}
		}
		$query->orderByRaw("FIELD(status , 'pending', 'viewed', 'processed') ASC");
		$query->orderBy('updated_at', 'DESC');

		return DataTables::eloquent($query)
		->filter(function ($filter) use ($request) {
				if ($request->has('document_type')) {
					if($request->get('document_type') != ''){
						$filter->where('document_type', '=', $request->get('document_type'));
					}
                }
				
				if ($request->has('client_id')) {
					if($request->get('client_id') != ''){
						$client_id = $request->get('client_id');
						$filter->whereHas('user', function($q) use ($client_id){
							$q->where('parent_user', $client_id);
						});
					}
                }
				
				if ($request->has('user_id')) {
					if($request->get('user_id') != ''){
						$user_id = $request->get('user_id');
						$filter->whereHas('user', function($q) use ($user_id){
							$q->where('id', $user_id);
						});
					}
                }
				
				if ($request->has('status')) {
					if($request->get('status') != ''){
						$filter->where('status', '=', $request->get('status'));
					}
                }
		})
		->addIndexColumn()
		->editColumn('status', function($document){
			if($document->status == 'pending'){
				return '<span class="text-bold" style="color: red;">Pending</span>';
			} else if($document->status == 'viewed'){
				return '<span class="text-bold" style="color: #605ca8;">Viewed</span>';
			}
			return '<span class="text-bold text-success">Processed</span>';
		})
		->addColumn('action', function($document){
			$buttons = '<a href="'.route('document.show', $document->id).'" class="btn btn-xs btn-success"><i class="glyphicon glyphicon-eye-open"></i> Show</a>';
			return $buttons;
		})
        ->rawColumns(['action', 'status'])
		->toJson();
    }
	
	public function show(Document $document)
	{
		/* $user = Auth::user();
		if($user->inRole('admin')) {
			$doc_user = User::find($document->user_id)->parent_client;
		} else if($user->inRole('client')){
			$doc_user = User::find($document->user_id)->parent_user;
		}
		
		if (Gate::denies('document-access', $doc_user)) {
			abort(404);
		} */
		
		if($document->status == 'pending'){
			$document->status = 'viewed';
			$document->timestamps = false;
			$document->save();
		}
		
		if($document->status == 'pending'){
		}
		
        return view('pages.document.show', compact('document'));
    }
	
	public function changeStatus(Request $request, Document $document)
	{
		if($document) {
			$document->status = $request->input('status');
			$document->timestamps = false;
			$document->save();
			return response()->json(['success' => true]);
		}
		
		return response()->json(['success' => false]);
    }
}