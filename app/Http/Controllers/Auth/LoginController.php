<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
	
	public function username()
    {
        $identity  = request()->get('identity');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }
	
	protected function validateLogin(\Illuminate\Http\Request $request)
    {
        $this->validate(
            $request,
            [
                'identity' => 'required|string',
                'password' => 'required|string',
            ],
            [
                'identity.required' => 'Username or email is required',
                'password.required' => 'Password is required',
            ]
        );
    }
	
	protected function sendFailedLoginResponse(\Illuminate\Http\Request $request)
    {
		$request->session()->flash('message.level', 'danger');
		$request->session()->flash('message.content', 'These credentials do not match our records.');
        throw ValidationException::withMessages(
            [
                'identity' => [trans('auth.failed')],
            ]
        );
    }
	
	public function login(\Illuminate\Http\Request $request) {
		$this->validateLogin($request);

		// If the class is using the ThrottlesLogins trait, we can automatically throttle
		// the login attempts for this application. We'll key this by the username and
		// the IP address of the client making these requests into this application.
		if ($this->hasTooManyLoginAttempts($request)) {
			$this->fireLockoutEvent($request);
			return $this->sendLockoutResponse($request);
		}

		// This section is the only change
		if ($this->guard()->validate($this->credentials($request))) {
			$user = $this->guard()->getLastAttempted();

			// Make sure the user is active
			if ($user->status == 'active' && $this->attemptLogin($request)) {
				// Send the normal successful login response
				return $this->sendLoginResponse($request);
			} else {
				// Increment the failed login attempts and redirect back to the
				// login form with an error message.
				$this->incrementLoginAttempts($request);
				
				$request->session()->flash('message.level', 'danger');
				$request->session()->flash('message.content', 'Your account is not active.');
				
				return redirect()
					->back()
					->withInput($request->only($this->username(), 'remember'));
			}
		}

		// If the login attempt was unsuccessful we will increment the number of attempts
		// to login and redirect the user back to the login form. Of course, when this
		// user surpasses their maximum number of attempts they will get locked out.
		$this->incrementLoginAttempts($request);

		return $this->sendFailedLoginResponse($request);
	}
	
	public function frontLogin(\Illuminate\Http\Request $request)
    {
		$user =  User::where('username', $request->get('username'))->get();
		if($user->isNotEmpty()){
			$user = User::find($user[0]->id);
			if(!$user->inRole('user')){
				return response()->json([
						"error" => "invalid_credentials",
						"message" => "You do not have access."
					], 401);
			}
			
			if($user->inRole('user') && $user->status == 'inactive'){
				return response()->json([
						"error" => "invalid_credentials",
						"message" => "You do not have access."
					], 401);
			}
		}
		
        // grab credentials from the request
        $credentials = $request->only('username', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    "error" => "invalid_credentials",
                    "message" => "The user credentials were incorrect. "
                ], 401);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json([
                "error" => "could_not_create_token",
                "message" => "Enable to process request."
            ], 422);
        }

        // all good so return the token
        $user =  User::where('username', $request->get('username'))->get();
        return response()->json([
            'user'  => $user,
            'token' => $token,
        ],200);

    }
	
	public function logout(\Illuminate\Http\Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/admin/login');
    }
}
