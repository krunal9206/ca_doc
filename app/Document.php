<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
	protected $fillable = [
        'user_id', 'name', 'document_type', 'upload_type', 'status'
    ];

    protected $hidden = [
        'user_id'
    ];
	
	public function getCreatedAtAttribute($date)
	{
		return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y h:i A');
	}
	
	public function getUpdatedAtAttribute($date)
	{
		return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y h:i A');
	}
	
	public function files() 
	{
        return $this->hasMany('App\DocumentFiles');
    }
	
	public function chats() 
	{
        return $this->hasMany('App\Chat');
    }
	
	public function user() 
	{
        return $this->belongsTo('App\User');
    }
}
