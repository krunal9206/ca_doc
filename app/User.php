<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'email', 'username', 'password', 'status', 'parent_user', 'parent_client', 'business_type', 'name_ppd', 'pan_no', 'gst_no', 'mobile', 'address'
    ];

    protected $hidden = [
        'password', 'remember_token', 'updated_at', 'parent_user', 'parent_client', 'created_at'
    ];
	
	public function staffAccess() 
	{
        return $this->hasMany('App\StaffAccess');
    }
	
	public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_users');
    }

    public function hasAccess(array $permissions) : bool
    {
        foreach ($this->roles as $role) {
            if($role->hasAccess($permissions)) {
                return true;
            }
        }
        return false;
    }

    public function inRole(string $roleSlug)
    {
        return $this->roles()->where('slug', $roleSlug)->count() == 1;
    }
	
	public function parentClient() { 
		return $this->belongsTo( 'App\User', 'parent_user', 'id'); 
	}
	
}
