<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $fillable = [
        'document_id', 'user_id', 'message', 'status'
    ];

    protected $hidden = [
        'document_id', 'updated_at'
    ];
	
	public function getCreatedAtAttribute($date)
	{
		return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $date)->format('d-m-Y h:i A');
	}
	public function user() 
	{
        return $this->belongsTo('App\User');
    }
	public function document() 
	{
        return $this->belongsTo('App\Document');
    }
}
