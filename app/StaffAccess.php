<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffAccess extends Model
{
    protected $fillable = [
        'user_id', 'client_id', 'document_type'
    ];
}
