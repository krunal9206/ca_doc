<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DocumentFiles extends Model
{
    protected $fillable = [
        'document_id', 'ori_name', 'file_name'
    ];

    protected $hidden = [
        'document_id', 'created_at', 'updated_at'
    ];
	
	public function document() { 
		return $this->belongsTo('App\Document'); 
	}
}
