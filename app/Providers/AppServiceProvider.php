<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Auth;
use App\Document;
use App\Chat;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /* View::composer('includes.sidebar', function($view){
			$user_id = Auth::user()->id;
			$query = Document::whereHas('user', function($q) use ($user_id){
				$q->where('parent_user', $user_id);
			})->where('status', 'unread')->count();
			
			$query_chat = Chat::whereHas('user', function($q) use ($user_id){
				$q->where('parent_user', $user_id);
			})->where('status', 'unread')->count();
			
			$view->with('counts', [
				'unread_doc' => $query,
				'unread_chat' => $query_chat,
			]);
		}); */
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
