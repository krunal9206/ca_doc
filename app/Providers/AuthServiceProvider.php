<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
		
        Gate::define('see-only-super', function ($user) {
			return $user->inRole('super');
		});
		Gate::define('see-only-admin', function ($user) {
			return $user->inRole('admin');
		});
		Gate::define('see-only-client', function ($user) {
			return $user->inRole('client');
		});
		Gate::define('see-only-staff', function ($user) {
			return $user->inRole('staff');
		});
		Gate::define('user-access', function ($user, $child_user) {
			return $user->id == $child_user->parent_user OR $user->id == $child_user->parent_client;
		});
		Gate::define('document-access', function ($user, $document) {
			return $user->id == $document;
		});
    }
}
